<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tokenFirebase;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    public function getTokenFirebase(): ?string
    {
        return $this->tokenFirebase;
    }

    public function setTokenFirebase(?string $tokenFirebase): self
    {
        $this->tokenFirebase = $tokenFirebase;

        return $this;
    }
}
