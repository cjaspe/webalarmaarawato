<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Modificacion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ModificacionController extends AbstractController
{
    /**
     * @Route("/app/modificaciones", name="modificaciones", methods={"GET"})
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Modificacion::class);
        $modificaciones = $repository->findAll();

        return $this->render('modificacion/modificaciones.html.twig', [
            'modificaciones' => $modificaciones,
        ]);
    }

    /**
     * @Route("/app/modificacion", name="modificacion", methods={"POST"})
     */
    public function addModificacionAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $tipo = $request->get('tipomodificacion');
        $valor = $request->get('modificacion');
        $now = new \DateTime();

        $newModificacion = new Modificacion();
        $newModificacion->setTipo($tipo);
        $newModificacion->setValor($valor);
        $newModificacion->setCreatedAt($now);

        $entityManager->persist($newModificacion);
        $entityManager->flush();

        return $this->redirectToRoute('modificaciones');
    }

    /**
     * @Route("/app/modificacion-horario", name="modificacionHorario", methods={"POST"})
     */
    public function addModificacionHorarioAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $horaInicio = $request->get('hora_inicio');
        $horaFinal = $request->get('hora_final');
        $now = new \DateTime();

        $newModificacion = new Modificacion();
        $newModificacion->setTipo("Horario de Trabajo");
        $newModificacion->setValor("Desde las $horaInicio , Hasta las $horaFinal");
        $newModificacion->setCreatedAt($now);

        $entityManager->persist($newModificacion);
        $entityManager->flush();

        return $this->redirectToRoute('modificaciones');
    }
}
