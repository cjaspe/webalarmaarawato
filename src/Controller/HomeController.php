<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Alerta;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends AbstractController
{
    /**
     * @Route("/app/home", name="home", methods={"GET"})
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Alerta::class);

        $now = new \DateTime();
        $hoy = $now->format('Y-m-d');
        $mes = $now->format('Y-m-00');
        $anno = $now->format('Y-00-00');

        $alertasPromedio = $repository->findAllGroupBy('FECHA');
        $alertasHoy = $repository->findAllByDateUp($hoy);
        $alertasMes = $repository->findAllByDateUp($mes);
        $alertasAnno = $repository->findAllByDateUp($anno);
        $alertasTotal = $repository->findAll();

        return $this->render('home/home.html.twig', [
            'alertasPromedio' => $alertasPromedio,
            'alertasHoy' => $alertasHoy,
            'alertasMes' => $alertasMes,
            'alertasAnno' => $alertasAnno,
            'alertasTotal' => $alertasTotal,
        ]);

        /* return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]); */
    }

    /**
     * @Route("/api/home", name="homeApp", methods={"GET"})
     */
    public function indexApp()
    {
        $repository = $this->getDoctrine()->getRepository(Alerta::class);

        $now = new \DateTime();
        $hoy = $now->format('Y-m-d');
        $mes = $now->format('Y-m-00');
        $anno = $now->format('Y-00-00');

        $alertasPromedio = $repository->findAllGroupBy('FECHA');
        $alertasHoy = $repository->findAllByDateUp($hoy);
        $alertasMes = $repository->findAllByDateUp($mes);
        $alertasAnno = $repository->findAllByDateUp($anno);
        $alertasTotal = $repository->findAll();

        return new JsonResponse([
            'success' => true,
            'alertasPromedio' => $alertasPromedio,
            'alertasHoy' => $alertasHoy,
            'alertasMes' => $alertasMes,
            'alertasAnno' => $alertasAnno,
            'alertasTotal' => $alertasTotal,
        ]);

    }
}
