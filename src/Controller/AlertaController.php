<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Alerta;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class AlertaController extends AbstractController
{
    /**
     * @Route("/app/alertas", name="alertas", methods={"GET"})
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Alerta::class);
        $alertas = $repository->findBy([],['id' => 'DESC']);

        return $this->render('alerta/alertas.html.twig', [
            'alertas' => $alertas,
        ]);
    }

    /**
     * @Route("/app/alerta", name="alerta", methods={"POST"})
     */
    public function addAlertaAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $remitente = $request->get('remitente');
        $tipoalerta = $request->get('tipoalerta');
        $sensor = $request->get('sensor');
        $now = new \DateTime();

        $newModificacion = new Alerta();
        $newModificacion->setRemitente($remitente);
        $newModificacion->setTipo($tipoalerta);
        $newModificacion->setZona($sensor);
        $newModificacion->setFecha($now);
        $newModificacion->setHora($now);

        $entityManager->persist($newModificacion);
        $entityManager->flush();

        /* Envio de notificacion PUSH */

        $users = $entityManager->getRepository(User::class)->findAll();
        $title = 'Alerta de Intruso';
        $body = 'Se ha detectado una alerta de intruso en Arawato Media C.A.';

        foreach ($users as $user) {
            $token = $user->getTokenFirebase();
            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array(
                'to' => $token,
                // 'to' => 'd_VQ3ptpt84:APA91bEOz4aI9kaHRiATkvkiL9AEmtAZHBPUisuexIgGK2rZWC86RJp7-MG8H52rrX8k2bF0doxSlFyWFvDdYmsxivWemMuYxIVbSD9mREIIqDLfVgJJb7V-vmtO909g1b2GS1iGX0X8',
                'notification' => ['body' => $body, 'title' => $title, "icon" => "ic_logo", 'color' => "#000", "vibrate" => 300, 'sound' => "default", "priority" => "high", "large_icon" => "ic_logo"],
                'data' => [
                    'type' => 'rate', 
                    "dataTitle" =>  $title,
                    "dataBody" =>  $body
                ]
            );
            $headers = array(
                'Authorization:key=AAAANSiUhVs:APA91bEIOGGwzwNC31TZKrhu9WCtLPJ8tjUnIPJfJvTqu0QQVbSGI1w62bkdsX1b6kvjENYowASPcWEF0X43pJRASlAC4N38EohJzXfECC5IkwkzsrMVmzBKfdMDcn8yk3OruUh7CnRJ',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
        }

        /* Fin de notificacion PUSH */

        return $this->redirectToRoute('home');
    }
}
