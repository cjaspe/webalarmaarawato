<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends AbstractController
{
    /**
     * @Route("/api/token-user", name="token-user", methods={"POST"})
     */
    public function addTokenFbUser(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $username = $request->get('username');
        $token_firebase = $request->get('token');

        if (!$username) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Usuario no incluido'
            ]);
    
        }
        if (!$token_firebase) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Token no incluido'
            ]);
        }

        $user = $entityManager->getRepository(User::class)->findOneBy(['username' => $username]);

        if (!$user) {
            return new JsonResponse([
                'success' => false,
                'message' => 'El usuario no existe'
            ]);
        }

        $user->setTokenFirebase($token_firebase);
        $entityManager->flush();

        return new JsonResponse([
            'success' => true,
            'message' => 'Token actualizado satisfactoriamente'
        ]);
    }
}
