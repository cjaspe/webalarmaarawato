<?php

namespace App\Repository;

use App\Entity\Alerta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Alerta|null find($id, $lockMode = null, $lockVersion = null)
 * @method Alerta|null findOneBy(array $criteria, array $orderBy = null)
 * @method Alerta[]    findAll()
 * @method Alerta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlertaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Alerta::class);
    }

    // /**
    //  * @return Alerta[] Returns an array of Alerta objects
    //  */
    public function findAllGroupBy($value): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT $value, COUNT($value) FROM alerta GROUP BY $value";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return Alerta[] Returns an array of Alerta objects
    //  */
    public function findAllByDateUp($value): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT * FROM alerta WHERE fecha >= :fecha";

        $stmt = $conn->prepare($sql);
        $stmt->execute(['fecha' => $value]);

        return $stmt->fetchAll();
    }    

    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Alerta
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
