// Datos para conectar con MQTT
var server = "soldier.cloudmqtt.com";
var port = 38591;   
var usuario = 'Arawato_Charallave';
var contrasena = '12345678';
var clientId = "ws" + Math.random();

// Espera del reporte del Equipo
var timeReporte = (3*60);
function muestraReporte(){
    $(".class_estado").removeClass("text-green");
    $(".class_estado").removeClass("text-aqua");
    $(".class_estado").removeClass("text-yellow");
    $(".class_estado").addClass("text-red");
    document.getElementById("estado").innerHTML = "Desconectado";
}

function restar(){
    timeReporte = timeReporte-1;
    console.log('.');
    if(timeReporte == 0){
        clearInterval(temp); 
        muestraReporte();  
    } 
}
var temp = setInterval(restar, 1000);

// Funcion que envia el modo de funcionamiento que se establece
function setModo(dato){
    message = new Paho.MQTT.Message(dato);
    message.destinationName = '/' + usuario + '/modoSistema'
    client.send(message);
    
    swal({
        position: 'top-right',
        type: 'success',
        title: 'Cambio Aplicado!',
        showConfirmButton: false,
        timer: 1500
    });

    if (dato=="0") {
        $("#tipomodificacion").attr("value", "Modalidad de Funcionamiento");
        $("#modificacion").attr("value", "Apagar Sistema");
    }
    else if (dato=="1") {
        $("#tipomodificacion").attr("value", "Modalidad de Funcionamiento");
        $("#modificacion").attr("value", "Encender Siempre");
    }
    else if (dato=="2") {
        $("#tipomodificacion").attr("value", "Modalidad de Funcionamiento");
        $("#modificacion").attr("value", "Horario Programado");
    }
    
    document.enviaModificacion.submit();
    
};
	
// Funcion que envia la hora de funcionamiento que se establece
function setHora(){
    let hora_inicio = document.getElementById("hora_inicio").value;
    let hora_final = document.getElementById("hora_final").value;
    let dato = hora_inicio+hora_final;

    if ((hora_inicio == "") || (hora_final == "")) {
        swal({
                position: 'top-right',
                type: 'error',
                title: 'Error Al Establecler !',
                text: 'Debe completar correctamente los campos del horario.',
                showConfirmButton: false,
                timer: 5000
            });
    }
    else {
    message = new Paho.MQTT.Message(dato);
    message.destinationName = '/' + usuario + '/horarioSistema'
    client.send(message);
    
    swal({
        position: 'top-right',
        type: 'success',
        title: 'Cambio Aplicado!',
        showConfirmButton: false,
        timer: 1500
    });
    document.enviaHorario.submit();
    }
}
	
// called when the client connects
function onConnect() {
    // Once a connection has been made, make a subscription and send a message.
    console.log("onConnect");
    client.subscribe("#");
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:", responseObject.errorMessage);
        setTimeout(function() { client.connect() }, 5000);
    }
}

// called when a message arrives
function onMessageArrived(message) {
    if (message.destinationName == '/' + usuario + '/' + 'reporteSistema') { //acá coloco el topic
        timeReporte = (3*60);
        /* console.log(message.payloadString[0]); */
        if (message.payloadString[0] == 0) {
            $(".class_estado").removeClass("text-green");
            $(".class_estado").removeClass("text-aqua");
            $(".class_estado").addClass("text-yellow");
            document.getElementById("estado").innerHTML = "Apagado";
        }
        else if (message.payloadString[0] == 1) {
            $(".class_estado").removeClass("text-aqua");
            $(".class_estado").removeClass("text-yellow");
            $(".class_estado").addClass("text-green");
            document.getElementById("estado").innerHTML = "Encendido Siempre";
        }
        else if (message.payloadString[0] == 2) {
            $(".class_estado").removeClass("text-green");
            $(".class_estado").removeClass("text-yellow");
            $(".class_estado").addClass("text-aqua");
            document.getElementById("estado").innerHTML = "Horario Programado <br>&nbsp&nbsp&nbsp&nbsp&nbsp" + message.payloadString.slice(2);
        }
    }

    if (message.destinationName == '/' + usuario + '/' + 'Deteccion_Movimiento') { //acá coloco el topic
        
        $("#remitente").attr("value", usuario);
        $("#tipoalerta").attr("value", "Deteccion_Movimiento");
        $("#sensor").attr("value", message.payloadString);
        document.enviaDatos.submit();
    }
}

function onFailure(invocationContext, errorCode, errorMessage) {
    $(".class_estado").removeClass("text-green");
    $(".class_estado").removeClass("text-aqua");
    $(".class_estado").removeClass("text-yellow");
    $(".class_estado").addClass("text-red");
    document.getElementById("estado").innerHTML = " Sin Conexion";
}
            
// Create a client instance
var client = new Paho.MQTT.Client(server, port, clientId);
            
// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
            
// connect the client
client.connect({
    useSSL: true,
    userName: usuario,
    password: contrasena,
    onSuccess: onConnect,
    onFailure: onFailure
}); 
