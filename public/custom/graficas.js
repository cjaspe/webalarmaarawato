var color = Chart.helpers.color;
var barChartData = {
	labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
	datasets: [{
		label: 'Registros',
		backgroundColor: 'rgba(52, 73, 94, 0.5)',
		borderColor: 'rgb(52, 73, 94)',
		borderWidth: 1,
		data: [12, 19, 3, 5, 2, 3]
	}]

};

window.onload = function() {
	var ctx = document.getElementById("myCanvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            legend: {
                position: 'top',
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }],
            },
            title: {
                display: true,
                text: 'INTRUSIONES REGISTRADAS',
                fontSize: 15,
            }
        }
    });
};
	
var colorNames = Object.keys(window.chartColors);
